<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="A collection of resources for learning about 
        becoming a UX designer.">
  <title>
    UX Resources
  </title>
        <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
    </head>
    <body>
        
        <nav class="">
            @include('_partials.header')
        </nav>

        <div class="flex justify-center w-full mt-10">
            <div class="w-9/10 lg:w-3/5">

                <main class="">
                    <div class="">
                        @yield('content')
                    </div>
                </main>

            </div>
        </div>

        <footer class="">
            @include('_partials.footer')
        </footer>

    </body>
</html>
