@php 

@endphp 


@extends('_layouts.master')

@section('content')

    @foreach ($ux_categories as $ux_category)
        <div class="py-6 px-4 lg:px-24 mb-10 ">
            <h2 class="text-3xl font-bold text-blue-600 mb-4" id="{{ $ux_category->name }}">{{ $ux_category->name }}</h2>
            @foreach ($ux_items as $ux_item)
                @if ($ux_category->name === $ux_item->category)
                    <div class="mb-2">
                        @if ($ux_item->best)
                        <span class="font-bold text-blue-500">BEST</span>
                        @endif
                        <a class=" font-serif text-black hover:text-blue-500"
                        href="{{ $ux_item->url }}"
                        target="_blank"
                        rel="noopener noreferrer"
                        >
                            {{ $ux_item->title  }}
                        </a>
                    </div>
                    @if ($ux_item->excerpt)
                        <div class="mb-2 font-serif text-gray-600">
                            {!! $ux_item->excerpt !!}
                        </div>
                    @endif
                @endif
            @endforeach
        </div>
    @endforeach

@endsection


