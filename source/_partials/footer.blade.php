<div class="py-24 bg-tan mt-2 lg:mt-12 bg-blue-300">
    <div class="flex justify-center w-full">
        <a href="https://bob-humphrey.com/">
            <img class="w-16 m-auto" src="/assets/images/bh-logo.gif" alt="Bob Humphrey website ">
        </a>
    </div>
</div>