<div class="lg:flex mb-3 bg-blue-300 ">

    <div class="w-full px-2">
        <h1 class="text-center text-3xl lg:text-5xl font-bold text-blue-800 pt-6 lg:pt-6">UX Resources </h1>
        <div class="lg:flex items-center justify-center text-center text-xl lg:text-3xl text-white pt-4 pb-8">
            @foreach ($ux_categories as $ux_category)
                <div class="px-2">
                    <a href={{ "#" . $ux_category->name }}>
                        {{ $ux_category->name }}
                    </a>
                </div>
            @endforeach
        </div>
    </div>

</div>