<?php
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

return [
    'production' => true,
    'baseUrl' => '',
    'collections' => [
        'ux_categories' => [
            'items' => function() {
                $cockpit_token = getenv('COCKPIT_TOKEN');
                $UxCategoriesApiUrl = 'https://cockpit.bob-humphrey.com/api/collections/get/ux_categories?token=' . $cockpit_token;
                $data = json_decode(file_get_contents($UxCategoriesApiUrl));
                $categories = collect($data->entries)->map(function ($category) {
                    return [
                        'sort' => $category->sort,
                        'name' => $category->name
                    ];
                });
                return $categories;  
            },
            'sort' => 'sort',
        ],
        'ux_items' => [
            'items' => function() {
                $cockpit_token = getenv('COCKPIT_TOKEN');
                $UxItemsApiUrl = 'https://cockpit.bob-humphrey.com/api/collections/get/ux_items?token=' . $cockpit_token;
                $data = json_decode(file_get_contents($UxItemsApiUrl));
                $items = collect($data->entries)->map(function ($item) {
                    return [
                        'category' => $item->category->display,
                        'title' => $item->title,
                        'url' => $item->url,
                        'excerpt' => $item->excerpt
                    ];
                });
                return $items;  
            },
            'sort' => 'title',
        ],
    ],
];
